import re

def read_section(title, filename):
    fo = open(filename,'r')
    section = ""
    flag='off'
    for line in fo:
        if line.startswith(title):
            flag = 'on'
            #section += line
        elif flag =='on':
            if len(line.strip()) == 0:  
                #section += line
                flag = 'off'
            else:
                section += line
    fo.close()    
    return section

def atom2mass(atom):
    if atom == 'C':
        mass = 12.0110
    elif atom == 'O':
        mass = 15.9990
    elif atom == 'H':
        mass = 1.0080
    return mass

def convert_atoms(atoms):
    section = "[ atoms ]\n"
    section += ";   nr  type  resi  res  atom  cgnr     charge      mass       ; qtot   bond_type\n"
    for i in range(len(atoms.split('\n'))-1):
        column = atoms.split('\n')[i].split()
        atom = atoms.split('\n')[i].split()[0][0]
        charge = float(atoms.split('\n')[i].split()[4])
        mass = atom2mass(atom)
        comment = ''.join(column[8:])
        section += "{0:5d}   opls_{1}      1    UNK   {2:s}{3:02}    {4:2d}    {5:10.6f}    {6:7.4f} ; {7}\n".format(i+1,int(column[1]),atom,i+1,i+1,charge,mass,comment)
        #print("%5d opls_%3d" %(i,int(column[1])))
    return section

def convert_bonds(bonds):
    section += "[ bonds ]\n"
    section += ";   ai     aj funct   r             k\n"
    for i in range(len(bonds.split('\n'))-1):
        column = bonds.split('\n')[i].split()
        atomA = int(re.sub("\D","",column[0]))
        atomB = int(re.sub("\D","",column[1]))
        k = float(column[2])*4.184*100*2 
        r = float(column[3])*0.1 # A to nm 
        section += "{0:2d}  {1:2d}   1  {2:6.4f}  {3:10.3f}\n".format(atomA, atomB, r, k)
    return section

def convert_angles(angles):
    section = "[ angles ]\n"
    section += ";  ai    aj    ak funct  theta0    k\n"
    for i in range(len(angles.split('\n'))-1):
        column = angles.split('\n')[i].split()
        atomA = int(re.sub("\D","",column[0]))
        atomB = int(re.sub("\D","",column[1]))
        atomC = int(re.sub("\D","",column[2]))
        k = float(column[3])*4.184*2
        theta = float(column[4])
        section += "{0:2d}    {1:2d}    {2:2d}    1    {3:7.3f}    {4:7.3f}\n".format(atomA,atomB,atomC,theta,k)
    return section

def convert_imporper(improper_diherdrals):
    section = "[ dihedrals ]\n"
    section += "; IMPROPER DIHEDRAL ANGLES\n"
    for i in range(len(improper_diherdrals.split('\n'))-1):
        column = improper_diherdrals.split('\n')[i].split()
        ai = int(re.sub("\D","",column[0]))
        aj = int(re.sub("\D","",column[1]))
        ak = int(re.sub("\D","",column[2]))
        al = int(re.sub("\D","",column[3]))
        theta = 180.00
        k = float(column[4])*4.184*0.5
        section += "{0:2d}    {1:2d}    {2:2d}    {3:2d}   {4:7.3f}    {5:7.3f}\n".format(ai,aj,ak,al,thata,k)
    return section


filename='test.ffld'

atoms = read_section('OPLSAA FORCE FIELD TYPE ASSIGNED',filename)

bonds = read_section(' Stretch            k            r0    quality         bt        comment',filename)

angles = read_section(' Bending                      k       theta0    quality   at  comment', filename)

improper_diherdrals = read_section(' improper Torsion                   V2    quality  comment', filename)



    
